const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const globby = require('globby');
const eslint = require('eslint/lib/cli');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const browserify = require('browserify');
const watchify = require('watchify');
const uglify = require('gulp-uglify');
const babel = require('babelify');
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const plumber = require('gulp-plumber');
const template = require('gulp-template');
const gulpif = require('gulp-if');
const htmlmin = require('gulp-htmlmin');
const runSequence = require('gulp-run-sequence');
const del = require('del');
const util = require('gulp-util');
const sass = require('gulp-sass');
const minifyCSS = require('gulp-minify-css');
const watch = require('gulp-watch');

// constants
const PORT = 3000;
const BUILD_PATH = path.join(__dirname, 'build');

// error handling
function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

// eslint
function runEslint(patterns) {
  globby(patterns).then((paths) => {
    const eslintErrors = eslint.execute(paths.join(' '));

    console.log('- - Running ESLint - -');

    if (eslintErrors) {
      console.log((new Error('ESLint failed \uD83D\uDE1E')).toString());
    } else {
      console.log('ESLint done, looking good! \uD83D\uDC4D');
    }
  }).catch((err) => {
    console.log(err.toString());
    process.exit(1);
  });
}

// production env
function isProduction() {
  return !!util.env.production;
}

// build clean
gulp.task('build-clean', (callback) => {
  return del(['./build/*'], callback);
});

// build markup
gulp.task('build-markup', () => {
  const version = process.env.CIRCLE_TAG || process.env.CIRCLE_SHA1 || Date.now();

  const options = {
    version,
    cssExt: isProduction() ? `min.css?v=${version}` : 'css',
    jsExt: isProduction() ? `min.js?v=${version}` : 'js'
  };

  return gulp.src('./source/**/*.html')
    .pipe(plumber({ errorHandler: handleError }))
    .pipe(template(options))
    .pipe(gulpif(isProduction(), htmlmin({ collapseWhitespace: true })))
    .pipe(gulp.dest('./build'));
});

// build javascripts
function buildScript(isWatching) {
  const b = watchify(browserify('./source/js/index.js', { debug: true }).transform(babel));

  if (!isProduction()) {
    runEslint('./client/source/js/**/*.js');
  }

  function rebundle() {
    b.bundle()
      .on('error', (err) => { console.error(err); this.emit('end'); })
      .pipe(gulpif(isProduction(), uglify()))
      .pipe(source('index.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./build/js'));
  }

  if (isWatching) {
    b.on('update', () => {
      console.log('-> bundling...');
      rebundle();
    });
  }

  rebundle();
}

// build stylesheets
gulp.task('build-stylesheets', () => {
  return gulp.src('./source/css/index.scss')
    .pipe(sass())
      .on('error', console.log)
    .pipe(gulpif(isProduction(), minifyCSS()))
    .pipe(gulp.dest('./build/css'));
});

// build assets
gulp.task('build-assets', () => {
  gulp.src('./source/**/*.{png,jpg,json,mp3,woff,svg}')
  .pipe(plumber({ errorHandler: handleError }))
  .pipe(gulp.dest('./build'));
});

// build all tasks
gulp.task('build', (callback) => {
  runSequence(
    'build-clean',
    'build-markup',
    'build-stylesheets',
    'build-assets',
    callback
  );
  buildScript(true);
});

// watch markup
gulp.task('watch-markup', () => {
  watch('./source/**/*.html', () => {
    gulp.start('build-markup');
  });
});

// watch stylesheets
gulp.task('watch-stylesheets', () => {
  watch('./source/css/**/*.scss', () => {
    gulp.start('build-stylesheets');
  });
});

// watch assets
gulp.task('watch-assets', () => {
  watch('./source/**/*.{png,jpg,json,mp3,woff,svg}', () => {
    gulp.start('build-assets');
  });
});

// watch all tasks
gulp.task('watch', (callback) => {
  runSequence(
    'watch-markup',
    'watch-stylesheets',
    'watch-assets',
    callback
  );
  buildScript(true);
});

gulp.task('serve', ['build'], () => {
  const app = express();

  app.use(morgan('dev'));
  app.use(express.static(BUILD_PATH));
  app.listen(PORT);

  console.log('Listening on port %d', PORT);
});

gulp.task('default', ['serve', 'watch']);
