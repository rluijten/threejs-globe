import * as THREE from 'three';

const OrbitControls = require('three-orbit-controls')(THREE);

export default class Globe {

  constructor () {
    this.width = window.innerWidth;
    this.height = window.innerHeight;

    this.init();
  }

  init () {
    // earth params
    this.radius = 0.5;
    this.segments = 32;
    this.rotation = 30;

    // scene
    this.scene = new THREE.Scene();

    // camera
    this.camera = new THREE.PerspectiveCamera(45, this.width / this.height, 0.01, 1000);
    this.camera.position.z = 2;

    // renderer
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setSize(this.width, this.height);

    // light
    this.scene.add(new THREE.AmbientLight(0x333333));

    this.light = new THREE.DirectionalLight(0xffffff, 1);
    this.light.position.set(5, 3, 5);
    this.scene.add(this.light);

    // sphere
    this.sphere = this.createGlobe(this.radius, this.segments);
    this.sphere.rotation.y = this.rotation;
    this.scene.add(this.sphere);

    // clouds
    this.clouds = this.createClouds(this.radius, this.segments);
    this.clouds.rotation.y = this.rotation;
    this.scene.add(this.clouds);

    // stars
    this.stars = this.createStars(90, 64);
    this.scene.add(this.stars);

    // controls
    this.controls = new OrbitControls(this.camera);

    document.body.appendChild(this.renderer.domElement);

    // render
    this.render();

    // resize
    window.addEventListener('resize', () => {
      this.onResize();
    }, false);
  }

  render () {
    this.controls.update();

    requestAnimationFrame(() => {
      this.render();
    });

    this.animate();

    this.renderer.render(this.scene, this.camera);
  }

  animate() {
    this.sphere.rotation.y += 0.0005;
    this.clouds.rotation.y += 0.0005;
  }

  createGlobe (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius, segments, segments),
      new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('img/textures/earth-no-clouds.jpg'),
        bumpMap: THREE.ImageUtils.loadTexture('img/textures/earth-bump.jpg'),
        bumpScale: 0.005,
        specularMap: THREE.ImageUtils.loadTexture('img/textures/earth-water.png')
      })
    );
  }

  createClouds (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius + 0.003, segments, segments),
      new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('img/textures/earth-clouds.jpg'),
        opacity: 0.5,
        transparent: true
      })
    );
  }

  createStars (radius, segments) {
    return new THREE.Mesh(
      new THREE.SphereGeometry(radius, segments, segments),
      new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture('img/textures/stars.jpg'),
        side: THREE.BackSide
      })
    );
  }

  onResize () {
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width, this.height);
  }
}
